// Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// Я би сказав, що це виконання коду не по порядку, не чекаючи завершення попередньої.

// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

let ipBtn = document.querySelector("#find-ip");
let root = document.querySelector("#root");
ipBtn.addEventListener("click", function () {
  getData();
});

async function getData() {
  let response1 = await fetch("https://api.ipify.org/?format=json");
  let data1 = await response1.json();

  let response2 = await fetch(
    `http://ip-api.com/json/${data1.ip}?fields=continent,country,region,city`
  );
  let data2 = await response2.json().then((data) => {
    let continent = data.continent;
    let continentParagraph = document.createElement("p");
    continentParagraph.textContent = "Континент: " + continent;

    let country = data.country;
    let countryParagraph = document.createElement("p");
    countryParagraph.textContent = "Країна: " + country;

    let region = data.region;
    let regionParagraph = document.createElement("p");
    regionParagraph.textContent = "Регіон: " + region;

    let city = data.city;
    let cityParagraph = document.createElement("p");
    cityParagraph.textContent = "Місто: " + city;

    root.append(
      continentParagraph,
      countryParagraph,
      regionParagraph,
      cityParagraph
    );
  });
}
